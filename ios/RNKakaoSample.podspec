
Pod::Spec.new do |s|
  s.name         = "RNKakaoSample"
  s.version      = "1.0.0"
  s.summary      = "RNKakaoSample"
  s.description  = <<-DESC
                  RNKakaoSample
                   DESC
  s.homepage     = "https://gitlab.com/dev409"
  s.license      = "MIT"
  # s.license      = { :type => "MIT", :file => "FILE_LICENSE" }
  s.author             = { "author" => "author@domain.cn" }
  s.platform     = :ios, "7.0"
  s.source       = { :git => "https://github.com/author/RNKakaoSample.git", :tag => "master" }
  s.source_files  = "*.{h,m}"
  s.requires_arc = true


  s.dependency "React"
  #s.dependency "others"

end

